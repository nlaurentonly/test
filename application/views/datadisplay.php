<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html>
<head>
<title>Display records</title>
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
</head>
 
<body>
<a href="<?php echo base_url('index'); ?>" class="right">Bock to Home</a>
<table width="600" border="0" cellspacing="5" cellpadding="5">
  <tr style="background:#CCC">
    <th>Index No</th>
    <th>First_name</th>
    <th>Last_name</th>
    <th>Country</th>
    <th>Subject</th>
    <th colspan="2">Option</th>
  </tr>
  <?php
  $i=1;
  foreach($datas as $row)
  {
  echo "<tr>";
  //$id=$row->regno;?>
  <td><?php echo $i; ?></td>
  <td><?php echo $row->fname; ?></td>
  <td><?php echo $row->lname; ?></td>
  <td><?php echo $row->country; ?></td>
  <td><?php echo $row->subject; ?></td>
  <td><a href="<?php echo base_url('updatedata');?>?id=<?php echo $row->id;?>" class="right">Edit</a></td><td><a href="<?php echo base_url('deletedata');?>?id=<?php echo $row->id;?>" class='right'>Delete</a></td>
  </tr>
 <?php $i++;
  }
   ?>
</table>
</body>
</html>