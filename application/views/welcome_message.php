<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Page Title</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  margin: 0;
}

.header {
  padding: 80px;
  text-align: center;
  background: #1abc9c;
  color: white;
}

.header h1 {
  font-size: 40px;
}


.navbar {
  overflow: hidden;
  background-color: gray;
}


.navbar a {
  float: left;
  display: block;
  color: white;
  text-align: center;
  padding: 14px 20px;
  text-decoration: none;
}

.navbar a.right {
  float: right;
}

.navbar a:hover {
  background-color: #ddd;
  color: black;
}
</style>
</head>
<body>
<div class="navbar">
  <a href="#"><i class="fa fa-home" style='font-size:20px;color:white;'></i></a>
  <a href="<?php echo base_url('registration'); ?>"><i class="fa fa-user-plus" style='font-size:20px;color:white;'></i></a>
  <a href="<?php echo base_url('about'); ?>">About</a>
  <a href="<?php echo base_url('displayrecords'); ?>">List of Users</a>
  <a href="<?php echo base_url('pdfreport'); ?>">Reports Using Mpdf</a>
  <a href="#" class="right">Sign in</a>
</div>
<div class="header">
  <h1>My Website</h1>
  <p>A website created by NLaurent.</p>
</div>



</body>
</html>
