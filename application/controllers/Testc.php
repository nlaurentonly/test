<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testc extends CI_Controller
{
    public function _construct(){
        parent::_construct();
       /*load database libray manually*/
	    $this->load->database();
    }
    public function index(){
        $this->load->view('welcome_message');
		
    }
    public function registration(){
		$this->load->view('registration');
	}
    /*Insert*/
	public function savedata()
	{
		/*load registration view form*/
		//$this->load->view('registration');
        /*load Model*/
        $this->load->model('Insert_model');
		/*Check submit button */
		if($this->input->post('save'))
		{
            $data['id']=$this->input->post('');
		    $data['fname']=$this->input->post('fname');
			$data['lname']=$this->input->post('lname');
			$data['country']=$this->input->post('country');
            $data['subject']=$this->input->post('subject');
			$response=$this->Insert_model->saverecords($data);
			if($response==true){
				redirect('displayrecords');
			}
			else{
				echo "Error !";
			}
		}
	}
    public function displayrecords(){
        $this->load->model('Display_model');
		$result['datas']=$this->Display_model->display_records();
        $this->load->view('datadisplay',$result);
    }

	public function updatedata()
	{
		
		$id=$this->input->get('id');
	 $this->load->model('Update_model');
	 //$id=$this->input->post('id');
	 $result['data']=$this->Update_model->displayrecordsById($id);
	 $this->load->view('dataupdate',$result);
	
		if($this->input->post('update'))
		{
		$id=$this->input->post('id');
		$fname=$this->input->post('fname');
		$lname=$this->input->post('lname');
		$country=$this->input->post('country');
		$subject=$this->input->post('subject');
		$this->Update_model->update_records($id,$fname,$lname,$country,$subject);
		redirect('displayrecords');
		}
	}
public function deletedata()
{
  $this->load->model('Delete_model');
  $id=$this->input->get('id');
  $response=$this->Delete_model->deleterecords($id);
  if($response==true){
    redirect('displayrecords');
}
  else{
    echo "Error !";
  }
}
public function pdfreport()
{
	    $this->load->model('Display_model');
		$result['datas']=$this->Display_model->displaympdfreports();
        $this->load->view('html_to_pdf',$result);
	$mpdf = new \Mpdf\Mpdf();
	$html = $this->load->view('html_to_pdf',[],true);
	$mpdf->WriteHTML($html);
	$mpdf->Output(); // opens in browser
}
}
	
